"use strict";

const ruloOfThree = (a, b, c) => {
  let x = "?";
  if (!isNaN(a) && !isNaN(b) && !isNaN(c)) {
    if (a <= 0 || b <= 0 || c <= 0) {
      return "Deben ser mayor que cero";
    } else {
      x = (b * c) / a;
      return x;
    }
  } else {
    return "Deben ser números";
  }
};

console.log(ruloOfThree(2, 8, 5));
// [2,3,8,0,0]
